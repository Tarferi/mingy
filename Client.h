#pragma once
#include "io.h"
#include "iostream"
#include "Chunk.h"
#include "Display.h"

void log(char* message);

class Client {
    public:
        Client(int descriptor, char* address);
        ~Client();

        bool messageAvailable();

        void handleMessage();

		void writePolicy();

		bool sendDisplayData(Display* d);

		void sendData(Chunk** chunks, unsigned short chunkCount);
		
		void sendDataChanged(Chunk** chunks, unsigned short* specifics, int specificCount);

		bool hasError();

		void setListPos(int lp);

		int getListPos();

    protected:

    private:
        int descriptor;
        char* address;

		int listPos;
		unsigned char* padding;
		unsigned short paddingLength;
		bool error;

		bool sendChunk(Chunk* chunk);
};

