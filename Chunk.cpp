#include "StdAfx.h"
#include "Chunk.h"

using namespace std;

Chunk::Chunk(unsigned char offX, unsigned char offY, int width, int height, int bytesPerPixel) {
	this->BPP=bytesPerPixel;
    this->hasData=false;
    this->error=false;
    this->offX=offX;
    this->offY=offY;
	this->offXPix=offX*width;
	this->offYPix=offY*height;
	this->width=width;
	this->height=height;
    this->totalSize=width*height;
	this->uncompressedData=(unsigned char*)malloc(this->totalSize*sizeof(unsigned char));
    if(!this->data) {
        this->error=true;
    }
	this->totalSize=width*height*this->BPP;
	this->data=(unsigned char*)malloc(this->totalSize*sizeof(unsigned char));
    if(!this->data) {
        this->error=true;
    }
	this->tmpData=(unsigned char*)malloc(this->totalSize*sizeof(unsigned char));
    if(!this->data) {
        this->error=true;
    }
	this->compressedData=(unsigned char*)malloc(this->totalSize*sizeof(unsigned char));
    if(!this->data) {
        this->error=true;
    }

	memset(this->data,0,this->totalSize);
	memset(this->tmpData,0,this->totalSize);

	this->offXPixEnd=this->offXPix+this->width;
	this->offYPixEnd=this->offYPix+this->height;

	this->realWidth=this->width*this->BPP;
}

int Chunk::getDataLength() {
	return this->compressedDataLength;
	//return this->totalSize;
}

void Chunk::copyImg(unsigned char* pixels) {

    BITMAPINFOHEADER bmih;
    bmih.biSize     = sizeof(BITMAPINFOHEADER);
	bmih.biWidth    = this->width;
	bmih.biHeight   = -this->height;
    bmih.biPlanes   = 1;
    bmih.biBitCount = 32;
    bmih.biCompression  = BI_RGB ;
    bmih.biSizeImage    = 0;
    bmih.biXPelsPerMeter    =   10;
    bmih.biYPelsPerMeter    =   10;
    bmih.biClrUsed    =0;
    bmih.biClrImportant =0;

    BITMAPINFO dbmi;
    ZeroMemory(&dbmi, sizeof(dbmi));  
    dbmi.bmiHeader = bmih;
    dbmi.bmiColors->rgbBlue = 0;
    dbmi.bmiColors->rgbGreen = 0;
    dbmi.bmiColors->rgbRed = 0;
    dbmi.bmiColors->rgbReserved = 0;

    HDC hdc = ::GetDC(NULL);

    HBITMAP hbmp = CreateDIBitmap(hdc, &bmih, CBM_INIT, pixels, &dbmi, DIB_RGB_COLORS);
    if (hbmp == NULL) {
        ::MessageBox(NULL, L"Could not load the desired image image", L"Error", MB_OK);
        return;
    }

    ::ReleaseDC(NULL, hdc);

    // a little test if everything is OK
    OpenClipboard(NULL);
    EmptyClipboard();
    SetClipboardData(CF_BITMAP, hbmp);
    CloseClipboard();

    // cleanup
    DeleteObject(hbmp);
}

bool Chunk::updateData(unsigned char* newData, unsigned char* oldData, int screenWidth, int screenHeight) {
	screenWidth*=this->BPP;
	for(int y=this->offYPix;y<this->offYPixEnd;y++) {
		if(y <= screenHeight) {
			memcpy(this->tmpData+(this->realWidth*(y-this->offYPix)),newData+(screenWidth*y)+(this->realWidth*this->offX),this->realWidth);
		}
	}
	//this->copyImg(this->tmpData);
	for(int i=0;i<this->totalSize;i++) {
		if(this->tmpData[i] != this->data[i]) {
			//cout << "Changed" << endl;
			unsigned char* tmp=this->data;
			this->data=this->tmpData;
			this->tmpData=tmp;
			this->compressNewData();
			return true;
		}
	}
	//cout << "Unchanged" << endl;
	/*
	if(memcmp(this->tmpData,this->data,this->totalSize) != 0) {
		unsigned char* tmp=this->data;
		this->data=this->tmpData;
		this->tmpData=tmp;
		this->compressNewData();
		return true;
	}
	*/
	return false;
}

void Chunk::compressNewData() {
	int size=this->width*this->height;
	int ni=0;
	for(int i=0;i<size;i++) {
		this->uncompressedData[i]=(this->data[ni]*0.2126)+(this->data[ni+1]*0.7152 )+(this->data[ni+2]*0.0722);
		ni+=this->BPP;
	}
	z_stream defstream;
	defstream.zalloc = Z_NULL;
	defstream.zfree = Z_NULL;
	defstream.opaque = Z_NULL;
	// setup "a" as the input and "b" as the compressed output
	
	defstream.avail_in = (uInt)size; // size of input, string + terminator
	defstream.next_in = (Bytef *)this->uncompressedData; // input char array

	//defstream.avail_in = (uInt)this->totalSize; // size of input, string + terminator
	//defstream.next_in = (Bytef *)this->data; // input char array

	defstream.avail_out = (uInt)this->totalSize; // size of output
	defstream.next_out = (Bytef *)this->compressedData; // output char array
    
	// the actual compression work.
	deflateInit(&defstream, Z_BEST_COMPRESSION);
	deflate(&defstream, Z_FINISH);
	deflateEnd(&defstream);
	this->compressedDataLength=defstream.total_out;
	//std::cout << "Compressed to " << this->compressedDataLength << " bytes from original " << size << " bytes" << std::endl;
}

Chunk::~Chunk() {
}

bool Chunk::hasError() {
    return this->error;
}

unsigned char Chunk::getOffX() {
    return this->offX;
}
unsigned char Chunk::getOffY() {
    return this->offY;
}

unsigned char* Chunk::getData() {
	return this->compressedData;
}