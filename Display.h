#pragma once

#include "Chunk.h"
#include "windows.h"
#include <math.h>
#include <stdio.h>

class Display {
    public:
        Display();
        ~Display();
        Chunk** getChunks();

        unsigned short* getChanges();
		unsigned short getChangedCount();

		unsigned short getScreenWidth();
		unsigned short getScreenHeight();


		bool hasError();
		bool update();
		unsigned short getChunkCount();

		int getChunkSizeX();
		int getChunkSizeY();

    protected:

    private:
		int totalChunks;
        void prepareChunks();
		Chunk** chunks;
        int ScreenX;
        int ScreenY;

		int BPP;
		int totalSize;

		unsigned char* data;
		unsigned char* tmpData;

		unsigned char* changedData;
		unsigned int changedDataLength;

		int chunkSizeX;
		int chunkSizeY;

		bool error;

		HDC hCaptureDC;
		HDC hDesktopDC;
		HBITMAP hCaptureBitmap;

		unsigned short* cahngedArr;
		unsigned short changedCount;
};

