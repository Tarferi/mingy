#include "StdAfx.h"
#include "Display.h"


using namespace std;

Display::Display() {
	this->error=false;
	this->chunkSizeX=144;
	this->chunkSizeY=150;
	this->BPP=4;


	RECT desktop;
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &desktop);
	this->ScreenX = desktop.right;
	this->ScreenY = desktop.bottom;


    this->prepareChunks();

	this->totalSize=this->ScreenX*this->ScreenY;
	this->data=(unsigned char*)malloc(this->totalSize*sizeof(unsigned char)*this->BPP);
    if(!this->data) {
        this->error=true;
    }
	this->tmpData=(unsigned char*)malloc(this->totalSize*sizeof(unsigned char)*this->BPP);
    if(!this->data) {
        this->error=true;
    }
	this->changedDataLength=(this->totalSize*this->BPP)+(this->totalChunks*2)+4;
	this->changedData=(unsigned char*) malloc(this->changedDataLength*sizeof(char));

    HWND hDesktopWnd = GetDesktopWindow();
    this->hDesktopDC = GetDC(hDesktopWnd);
    this->hCaptureDC = CreateCompatibleDC(this->hDesktopDC);
	this->hCaptureBitmap = CreateBitmap(this->ScreenX, this->ScreenY,1,this->BPP*8,NULL);
    SelectObject(this->hCaptureDC,this->hCaptureBitmap);
}

unsigned short Display::getScreenWidth() {
	return this->ScreenX;
}

unsigned short Display::getScreenHeight() {
	return this->ScreenY;	
}


int Display::getChunkSizeX() {
	return this->chunkSizeX;
}

int Display::getChunkSizeY() {
	return this->chunkSizeY;
}


 Chunk** Display::getChunks() {
	 return this->chunks;
 }

bool Display::hasError() {
	return this->error;
}

unsigned short Display::getChangedCount() {
	return this->changedCount;
}

unsigned short Display::getChunkCount() {
	return this->totalChunks;
}

unsigned short* Display::getChanges() {
	return this->cahngedArr;
}

bool Display::update() {
	bool update=false;
	this->changedCount=0;
	if (BitBlt(this->hCaptureDC,0,0,this->ScreenX,this->ScreenY, this->hDesktopDC,0,0,SRCCOPY|CAPTUREBLT) == 0) {
		this->error=true;
		return false;
	}
	/* EPIC DEBUG
	OpenClipboard(NULL);
    EmptyClipboard();
    SetClipboardData(CF_BITMAP, this->hCaptureBitmap);
    CloseClipboard();   
	*/
	GetBitmapBits(this->hCaptureBitmap,this->totalSize*this->BPP,this->tmpData);

	//this->chunks[5]->updateData(this->tmpData,this->data,this->ScreenX,this->ScreenY);
	//return false;

	
	for(int i=0;i<this->totalChunks;i++) {
		if(this->chunks[i]->updateData(this->tmpData,this->data,this->ScreenX,this->ScreenY)) {
			this->cahngedArr[this->changedCount]=i;
			this->changedCount++;
			update=true;
		}
	}
	unsigned char* tmp=this->data;
	this->data=this->tmpData;
	this->tmpData=tmp;
	return update;
}

typedef Chunk* ChunkPtr;

void Display::prepareChunks() {
	int chunksX=(int)ceil(((double)this->ScreenX/this->chunkSizeX));
	int chunksY=(int)ceil(((double)this->ScreenY/this->chunkSizeY));
	this->totalChunks=chunksX*chunksY;
	this->cahngedArr=(unsigned short*)malloc(this->totalChunks*sizeof(unsigned short));
	this->changedCount=0;
	if(!this->cahngedArr) {
		this->error=true;
		return;
	}
	ChunkPtr* test=(ChunkPtr*)malloc(this->totalChunks*sizeof(ChunkPtr*));
	if(!test) {
		this->error=true;
		return;
	}
	this->chunks=(Chunk**)test;
	for(int y=0;y<chunksY;y++) {
		for(int x=0;x<chunksX;x++) {
			int chunkIndex=(y*chunksX)+x;
			Chunk* chunk=new Chunk(x,y,this->chunkSizeX,this->chunkSizeY,this->BPP);
			if(chunk->hasError()) {
				this->error=true;
				return;
			}
			this->chunks[chunkIndex]=chunk;
		}
	}
}