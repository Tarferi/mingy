#pragma once


#include<stdio.h>
#include<string.h>

#include<sys/types.h>

#include "stdafx.h"
#include <winsock2.h>
#include <Ws2tcpip.h>
#include <stdio.h>
#include "iostream"
#include "conio.h"
#include <io.h>


#pragma comment(lib, "Ws2_32.lib")
using namespace std;
#include "Client.h"
#include "Display.h"
#include <vector>

void log(char* message);

class Server {
    private:
		int port;

		void createServerSocket();

		Client* acceptNewConnection();

    public:
        Server(int port);
        virtual ~Server();

    protected:

    private:
        int listener;
		Display* display;
		void mainLoop();
		void CommitQuery();
		void tryAcceptNewClients();
		std::vector<Client*> clients;
		void removeClientFromList(Client* c);
};



