#include "StdAfx.h"
#include "Client.h"


using namespace std;

Client::Client(int descriptor, char* address) {
    log("Client accepted");
    this->descriptor=descriptor;
    this->address=address;
	this->error=false;
	this->paddingLength=102400;
	this->padding=(unsigned char*)malloc(sizeof(unsigned char)*this->paddingLength);
	if(!this->padding){
		this->error=true;
		return;
	}
	memset(this->padding,' ',this->paddingLength);

	int send_buffer = 1;    // 64 KB
	int send_buffer_sizeof = sizeof(int);
	setsockopt(this->descriptor, SOL_SOCKET, SO_SNDBUF, (char*)&send_buffer, send_buffer_sizeof);

	bool nowait= true;
	int nowait_sizeof = sizeof(bool);
	setsockopt(this->descriptor, IPPROTO_TCP, TCP_NODELAY, (char*)&nowait, nowait_sizeof);

}

Client::~Client() {
    log("Closing client");
    closesocket(this->descriptor);
	if(this->padding){
		free(this->padding);
	}
}

void Client::writePolicy() {
	char* pl="<?xml version=\"1.0\"?><!DOCTYPE cross-domain-policy SYSTEM \"/xml/dtds/cross-domain-policy.dtd\"><cross-domain-policy><site-control permitted-cross-domain-policies=\"all\"/><allow-access-from domain=\"*\" to-ports=\"*\"/></cross-domain-policy>\0";
	int origLen=strlen(pl)+1;
	if (send(this->descriptor,(const char*)pl,origLen,0) == SOCKET_ERROR) {
		this->error=true;
	}
}

void Client::setListPos(int lp) {
	this->listPos=lp;
}

int Client::getListPos() {
	return this->listPos;
}

bool Client::messageAvailable(){
    return false;
}

void Client::handleMessage(){

}

void Client::sendDataChanged(Chunk** chunks, unsigned short* specifics, int specificCount) {
	for(int i=0;i<specificCount;i++){
		this->sendChunk(chunks[specifics[i]]);
	}
}

bool Client::hasError() {
	return this->error;
}

bool Client::sendDisplayData(Display* d) {
	unsigned char data[8];
	unsigned short chunkSizeX=(unsigned short)d->getChunkSizeX();
	unsigned short chunkSizeY=(unsigned short)d->getChunkSizeY();

	unsigned short screenX=d->getScreenWidth();
	unsigned short screenY=d->getScreenHeight();


	data[0]=(unsigned char)(chunkSizeX&0xff);
	data[1]=(unsigned char)((chunkSizeX>>8)&0xff);

	data[2]=(unsigned char)(chunkSizeY&0xff);
	data[3]=(unsigned char)((chunkSizeY>>8)&0xff);
	
	data[4]=(unsigned char)(screenX&0xff);
	data[5]=(unsigned char)((screenX>>8)&0xff);

	data[6]=(unsigned char)(screenY&0xff);
	data[7]=(unsigned char)((screenY>>8)&0xff);

	if (send(this->descriptor,(const char*)data,8,0) == SOCKET_ERROR) {
		this->error=true;
		return false;
	}
}

bool Client::sendChunk(Chunk* chunk) {
	unsigned char chunkData[5];
	chunkData[0]=chunk->getOffX();
	chunkData[1]=chunk->getOffY();
	unsigned short len=chunk->getDataLength();;
	chunkData[2]=(unsigned char)(len&0xff);
	chunkData[3]=(unsigned char)(len>>8);
	chunkData[4]=0;
	if (send(this->descriptor,(const char*)chunkData,5,0) == SOCKET_ERROR) {
		//wprintf(L"send failed with error: %d\n", WSAGetLastError());
		this->error=true;
		return false;
	}
	if (send(this->descriptor,(const char*)chunk->getData(),len,0) == SOCKET_ERROR) {
		this->error=true;
		return false;
	}
	return true;
}

void Client::sendData(Chunk** chunks, unsigned short chunkCount){
	for(int i=0;i<chunkCount;i++){
		this->sendChunk(chunks[i]);
	}
}
