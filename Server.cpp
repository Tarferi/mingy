#include "StdAfx.h"
#include "Server.h"



void log(char* message) {
	std::cout << message << endl;
}

Server::Server(int port) {
    this->port=port;
    this->createServerSocket();
	this->display=new Display();
	if(this->display->hasError()) {
		log("Display has errors");
		return;
	}
	log("Starting main loop");
	this->mainLoop();
	log("Main loop finished");
}

void Server::mainLoop() {
	DWORD start;
	DWORD stop;
	DWORD rate=1000;
	DWORD exec;
	DWORD toWait;
	
	while(true) {
		start=GetTickCount();
		this->CommitQuery();
		stop=GetTickCount();
		exec=stop-start;
		toWait=exec>rate?0:rate-exec;
		Sleep(toWait);
	}
}

void Server::tryAcceptNewClients() {
	Client* c=this->acceptNewConnection();
	if(c != NULL) {
		c->setListPos(this->clients.size());
		this->clients.push_back(c);
		c->writePolicy();
		c->sendDisplayData(this->display);
		c->sendData(this->display->getChunks(),this->display->getChunkCount());
		if(c->hasError()) {
			this->removeClientFromList(c);
			delete c;
		}
	}
}

void Server::removeClientFromList(Client* c) {
	std::vector<Client*>::iterator it = this->clients.begin();
	while(it != this->clients.end()) {
		Client* cc=*it;
		if(cc==c) {
			this->clients.erase(it);
			return;
		} else {
			++it;
		}
	}
}

void Server::CommitQuery() {
	this->tryAcceptNewClients();
	if (this->display->update()) {
		int changedCount=this->display->getChangedCount();
		//cout << "Changed: " << changedCount << endl;
		unsigned short* changedArr=this->display->getChanges();
		Chunk** chunks=this->display->getChunks();
		std::vector<Client*>::iterator it = this->clients.begin();
		while(it != this->clients.end()) {
			Client* c=*it;
			c->sendDataChanged(chunks,changedArr,changedCount);
			if(c->hasError()) {
				it=this->clients.erase(it);
				delete c;
			} else {
				++it;
			}
		}
	}
}

Server::~Server() {
}


void Server::createServerSocket() {
    log("Creating server socket");
    WSADATA wsaData;
    int iResult;
	iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed with error: %d\n", iResult);
    }
	int sock_desc = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sock_desc == INVALID_SOCKET) {
        printf("Cannot create socket!\n");
        return;
    }
    struct sockaddr_in server;  
    memset(&server, 0, sizeof(server));  
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;  
    server.sin_port = htons(this->port);  
    if (bind(sock_desc, (struct sockaddr*)&server, sizeof(server)) != 0) {
        printf("cannot bind socket!\n");
		close(sock_desc);  
        return;
    }
    if (listen(sock_desc, 20) != 0) {
        printf("cannot listen on socket!\n");
        close(sock_desc);  
        return;
    }
	this->listener=sock_desc;
	cout << "Server started on port " << this->port << endl;
	/*
	u_long iMode=1;
	iResult = ioctlsocket(this->listener, FIONBIO, &iMode);
		if (iResult != NO_ERROR) {
		printf("ioctlsocket failed with error: %ld\n", iResult);
	}
	*/
}


Client* Server::acceptNewConnection() {
	u_long iMode=1;
	int iResult = ioctlsocket(this->listener, FIONBIO, &iMode);
		if (iResult != NO_ERROR) {
		printf("ioctlsocket failed with error: %ld\n", iResult);
	}


	struct sockaddr_in client;  
    memset(&client, 0, sizeof(client));  
    socklen_t len = sizeof(client); 
    int client_desc = accept(this->listener, (struct sockaddr*)&client, &len);  
    if (client_desc == -1) {
		iMode=0;
		iResult = ioctlsocket(this->listener, FIONBIO, &iMode);
			if (iResult != NO_ERROR) {
			printf("ioctlsocket failed with error: %ld\n", iResult);
		}
        return NULL;
    }
	
	iMode=0;
	iResult = ioctlsocket(this->listener, FIONBIO, &iMode);
		if (iResult != NO_ERROR) {
		printf("ioctlsocket failed with error: %ld\n", iResult);
	}

	iResult = ioctlsocket(client_desc, FIONBIO, &iMode);
		if (iResult != NO_ERROR) {
		printf("ioctlsocket failed with error: %ld\n", iResult);
	}
	
	log("Accepted new client!");
	return new Client(client_desc,"Not yet implemented");
}
