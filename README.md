# Mingy #

Throwaway for somebody else to take over.

Basic code for desktop distribution. Server can handle up to 10 clients without affecting performance. The data are taken using WinGDI, which causes cursor blink, if windows with extended flags are required to appear in the result. Also, no enhancements are available (added realtime processing this project is considered potentially very dangerous).