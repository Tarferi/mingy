#pragma once

#include <iostream>
#include <io.h>
#include <Windows.h>
#define ZLIB_WINAPI

#include "zlib.h"

class Chunk {
    public:
        Chunk(unsigned char offX, unsigned char offY, int width, int height, int bytesPerPixel);
        ~Chunk();

        unsigned char getOffX();
        unsigned char getOffY();
        unsigned char* getData();
        bool hasError();
        bool updateData(unsigned char* newData, unsigned char* oldData, int screenWidth, int screenHeight);
		int getDataLength();
		
    protected:

    private:
		int width;
		int height;
        unsigned char offX;
        unsigned char offY;

		unsigned char* data;
		unsigned char* tmpData;

		int BPP;

        bool error;
        int totalSize;
        bool hasData;

		int offXPix;
		int offYPix;

		int offXPixEnd;
		int offYPixEnd;

		int realWidth;

		unsigned char* uncompressedData;
		unsigned char* compressedData;
		unsigned short compressedDataLength;

		void compressNewData();

		void copyImg(unsigned char* pixels);


};
